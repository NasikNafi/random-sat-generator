# Summary
This is a random SAT generator written in C++. It offers following three scaling parameters. 

1. Number of non-empty clauses that should occur in a formula, N. 
2. Maximum number of variables that can occur in a formula, K
3. Maximum size of each clause, L

Inaddition to that it provides a flag "--strict" to fix the size of each clause to L.


# Steps to build the code and generate CNF

1. Build the code:

    `g++ -std=c++11 main.cpp -o main`

2. Run the CNF generator:

    `./main -n <N> -k <K> -l <L>`
    
    To use the "--strict" flag run the code as follows:
    
    `./main -n <N> -k <K> -l <L> --strict`
    
3. You should see a file named "cnf\_instances\_\<currenttimeinmillis\>.cnf" in the same folder containing the generated cnf.
