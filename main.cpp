#include <iostream>
#include <list>
#include <fstream>
#include <sys/time.h>

using namespace std;

int main(int argc, char *argv[]) {

    int n, k, l, ll;
    bool strict = false;
    char file_name[255];

    // getting command line argument
    if (argc == 7 || argc == 8) {
        if (!strcmp(argv[1], "-n") && !strcmp(argv[3], "-k") && !strcmp(argv[5], "-l")) {
            n = atoi(argv[2]);
            k = atoi(argv[4]);
            l = atoi(argv[6]);
            if (argv[7] != NULL && !strcmp(argv[7], "--strict")) {
                strict = true;
            }
        } else {
            cout << "Arguments are not in correct format" << endl;
        }
    } else {
        cout << "Provide required number of arguments correctly." << endl;
    }

    // creating file
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    sprintf(file_name, "cnf_instance_%ld.cnf", ms);
    ofstream cnf_file(file_name);
    if (cnf_file.is_open()) {
        cnf_file << "c \n";
        cnf_file << "c In the first non comment line\n";
        cnf_file << "c the integers after \"p cnf\" are respectively\n";
        cnf_file << "c Number of non-empty clauses that should occur in a formula\n";
        cnf_file << "c Maximum number of variables that can occur in a formula\n";
        cnf_file << "c Maximum size of each clause\n";
        cnf_file << "c \n";
        cnf_file << "p cnf " << n << " " << k << " " << l << endl;
    } else cout << "Unable to open file";

    // generating cnf
    srand(time(NULL));
    for (int i = 0; i < n; ++i) {
        if (strict) {
            ll = l;
        } else {
            ll = rand() % l + 1;
        }

        std::list<int> literal_list = {};
        for (int j = 0; j < ll; ++j) {
            int literal;
            int var = rand() % k + 1;

            auto it_pos = std::find(literal_list.begin(), literal_list.end(), var);
            auto it_neg = std::find(literal_list.begin(), literal_list.end(), -var);
            if (it_pos != literal_list.end() || it_neg != literal_list.end()) {
                if (it_pos != literal_list.end()) {
                    literal = var;
                } else {
                    literal = -var;
                }
            } else {
                if (rand() % 2)
                    literal = -var;
                else
                    literal = var;
                literal_list.push_back(literal);
            }
            cnf_file << literal << " ";
        }
        cnf_file << 0 << "\n";
    }

    // closing file
    cnf_file.close();
    return 0;
}